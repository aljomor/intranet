'use strict';

module.exports = {
	app: {
		title: 'intranet',
		description: 'Intranet de Fundabit',
		keywords: 'Intranet, Fundabit'
	},
	port: process.env.PORT || 3001,
	templateEngine: 'swig',
	sessionSecret: 'FUNDABIT',
	sessionCollection: 'sessions',
	assets: {
		lib: {
			css: [
				'public/lib/bootstrap/dist/css/bootstrap.css',
				'public/dist/css/default.css',
                'public/lib/tr-ng-grid/trNgGrid.css',
                'public/dist/css/_vendor.min.css'
				//'public/lib/bootstrap/dist/css/bootstrap-theme.css',
			],
			js: [
				'public/lib/angular/angular.js',
				'public/lib/angular-resource/angular-resource.js',
				'public/lib/angular-cookies/angular-cookies.js',
				'public/lib/angular-animate/angular-animate.js',
				'public/lib/angular-touch/angular-touch.js',
                'public/lib/tr-ng-grid/trNgGrid.js',
				'public/lib/angular-sanitize/angular-sanitize.js',
				'public/lib/angular-ui-router/release/angular-ui-router.js',
				'public/lib/angular-ui-utils/ui-utils.js',
				'public/lib/angular-bootstrap/ui-bootstrap-tpls.js',
                'public/lib/angular-locale_es-es/angular-locale_es-es.js',
                'public/lib/jquery/dist/jquery.min.js',
                'public/lib/Grid-A-Licious/jquery.grid-a-licious.js'
                //'public/dist/js/scripts.js',
				//'public/dist/js/vendor.js'
			]
		},
		css: [
			'public/modules/**/css/*.css'
		],
		js: [
			'public/config.js',
			'public/application.js',
			'public/modules/*/*.js',
			'public/modules/*/*[!tests]*/*.js'
		],
		tests: [
			'public/lib/angular-mocks/angular-mocks.js',
			'public/modules/*/tests/*.js'
		]
	}
};
