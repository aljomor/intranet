'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users');
	var publicaciones = require('../../app/controllers/publicaciones');

	// Publicaciones Routes
	app.route('/publicaciones')
		.get(publicaciones.list)
		.post(users.requiresLogin, publicaciones.create);

	app.route('/publicaciones/:publicacioneId')
		.get(publicaciones.read)
		.put(users.requiresLogin, publicaciones.update)
		.delete(users.requiresLogin, publicaciones.hasAuthorization, publicaciones.delete);

  /*  app.route('publicaciones/:publicacioneId/comment/:commentId')
        .put(users.requiresLogin, publicaciones.hasAuthorization, publicaciones.updateComments); */


	// Finish by binding the Publicacione middleware
	app.param('publicacioneId', publicaciones.publicacioneByID);
};