'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    errorHandler = require('./errors'),
    Publicacione = mongoose.model('Publicacione'),
    _ = require('lodash');

/**
 * Create a Publicacione
 */
exports.create = function(req, res) {
    var publicacione = new Publicacione(req.body);
    publicacione.user = req.user;

    publicacione.save(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(publicacione);
        }
    });
};

/**
 * Show the current Publicacione
 */
exports.read = function(req, res) {
    res.jsonp(req.publicacione);
};

/**
 * Update a Publicacione
 */
exports.update = function(req, res) {
    var publicacione = req.publicacione ;

    publicacione = _.extend(publicacione , req.body);
   // console.log(req.body);

    publicacione.save(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(publicacione);
        }
    });
};

/**
 * Delete an Publicacione
 */
exports.delete = function(req, res) {
    var publicacione = req.publicacione ;

    publicacione.remove(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(publicacione);
        }
    });
};

/**
 * List of Publicaciones
 */
exports.list = function(req, res) { Publicacione.find().sort('-created').populate('user', 'displayName').populate('comments.comment').deepPopulate('comments.comment.user').exec(function(err, publicaciones) {
    if (err) {
        return res.status(400).send({
            message: errorHandler.getErrorMessage(err)
        });
    } else {
        res.jsonp(publicaciones);
    }
});
};

/**
 * Publicacione middleware
 */
exports.publicacioneByID = function(req, res, next, id) {
    Publicacione
        .findById(id)
        .populate('user', 'displayName')
        .populate('comments.comment')
        .deepPopulate('comments.comment.user')
        .exec(function(err, publicacione) {

                if (err) return next(err);
                if (! publicacione) return next(new Error('Failed to load Publicacione ' + id));
                req.publicacione = publicacione ;
                next();
        });
};

/**
 * Publicacione authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
    //console.log(req);
    if (req.publicacione.user.id !== req.user.id) {
        return res.status(403).send('User is not authorized');
    }
    next();
};