'use strict';

/**
 * Module dependencies.
 */
var deepPopulate = require('mongoose-deep-populate');
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Publicacione Schema
 */
var PublicacioneSchema = new Schema({

    /*contenido de la publicaion*/

    content: {
        type: String,
        default: '',
        required: 'Please fill Publicacion content',
        trim: true
    },
    /*cuando fue creada la publicacion*/

    created: {
        type: Date,
        default: Date.now
    },
    /*usuario creador de la publicacion*/

    users: {
        type: Schema.ObjectId,
        ref: 'User'
    },

    /*tipo de publicacion que el usuario quiera realizar*/

    type: {
        type: String,
        enum: ['regular', 'images', 'videos','files']
    },
    /*Etiquetar usuarios */

    tags: {
        users: [{
            user: {
                type: Schema.ObjectId,
                ref: 'User'
            }
        }]
    },
   /*guarda el url del archivo*/

    files: {
        file: [{
            url: {
                type: String
            },
            description: {
                type: String,
                default: ''
            }
        }]
    },
    /*comentarios se mostraran los ultimos 4 de cada publicacion*/

    comments: [{
        comment:{
            type: Schema.ObjectId,
            ref: 'Comment'
        }
    }],

   /*privacidad de la publicacion*/

    privacy: {
        type: {
            type: String,
            enum: ['public', 'team', 'personalized']
        },
        users: [
            {
                user: {
                    type: Schema.ObjectId,
                    ref: 'User'
                }
            }
        ],
        teams: [
            {
                team: {
                    type: Schema.ObjectId,
                    ref: 'Team'
                }
            }
        ]

    },

    /*darle me gusta a la publicacion*/

    likes:[{
       users:{
            type: Schema.ObjectId,
            ref: 'User'
        },

        status:{
            type: Boolean,
            default: true
        }
    }]

});

PublicacioneSchema.plugin(deepPopulate, {
    whitelist: [
        'user',
        'comments.comment.user'
    ]
} /* more on options below */);





mongoose.model('Publicacione', PublicacioneSchema);

