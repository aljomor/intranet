'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Notification Schema
 */
var NotificationSchema = new Schema({
    created: {
      type: Date,
      default: Date.now
    },
    /* Publicacion a la cual se le dio Me gusta o comentaron, entre otras. */
    post: {
      type: Schema.ObjectId,
      ref: 'Publicacione'
    },
    /* Usuario que Creo, le dio Me gusta o Comento una publicacion, generando asi la notificacion. */
    user: {
      type: Schema.ObjectId,
      ref: 'User'
    },
    /* A que pertenece la Notificacion. Ej: Publicaciones Propias, Actividad Grupos, Etiquetas, entre otras. */
    section: {
      type : String,
      enum: ['my_activity', 'group', 'tag', 'notice']
    },
    /* Accion que se Notifica. Ej: Me gusta, Comentario, entre otras. */
    action: {
      type : String,
      enum: ['like', 'comment', 'shared', 'wrote']
    },
    /* Usuario al cual le llega la notificacion. */
    receiver: {
      type: Schema.ObjectId,
      ref: 'User'
    },
    /* Indica si la notificacion fue vista. */
    seen: {
      type: Boolean,
      default: false
    },
    /* Indica si la notificacion esta visible. */
    status: {
      type: Boolean,
      default: true
    }
});

mongoose.model('Notification', NotificationSchema);