'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
 * Group Schema
 */
var GroupSchema = new Schema({
    nombre: {
        type: String,
        default: '',
        required: 'Por favor Introduzca el nombre del Grupo',
        trim: true
    },
    descripcion: {
        type: String,
        default: '',
        required: 'Por favor Introduzca la descripción del grupo',
        trim: true
    },
    rango: {
        type: Number,
        min: 1,
        max: 100,
        default: '',
        required: 'Por favor Introduzca el rango del usuario'
    },
    acceso: {
        type: Schema.Types.Mixed,
        required: 'Por favor Introduzca el acceso del usuario'
    },
    created: {
        type: Date,
        default: Date.now
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    status: {
        type: String,
        ref: 'User'
    }
});

mongoose.model('Group', GroupSchema);