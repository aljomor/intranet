'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Comment Schema
 */
var CommentSchema = new Schema({
/*contenido del comentario*/

    content: {
		type: String,
		default: '',
		required: 'Please fill Comment name',
		trim: true
	},
    /*contenido del comentario*/

    post: {
        type: Schema.ObjectId,
        ref: 'Publicacione'
    },
/*fecha de cuando fue creado el comentario*/

    created: {
		type: Date,
		default: Date.now
	},
/*usuario que genero o comento*/

    user: {
		type: Schema.ObjectId,
		ref: 'User'
	},
/*"me gusta" de los usuarios */

    likes:[{
        users:{
            type: Schema.ObjectId,
            ref: 'User'
        },

        status:{
            type: Boolean,
            default: true
        }
    }],
/*personas etiquetadas en el comentario */

    tags: {
        users: [{
            user: {
                type: Schema.ObjectId,
                ref: 'User'
            }
        }]
    },
/*usuario y fecha de la ultima modificacion del comentario*/

    modified:{
            user: {
                type: Schema.ObjectId,
                ref: 'User'
            },
            date:{
                type: Date,
                default: Date.now //Discutir acerca de el "Date.now"
            }
        },
/*status del comentario*/

    status:{
        type: String,
        enum:['active', 'deleted', 'banned', 'spam']
    }
});

mongoose.model('Comment', CommentSchema);