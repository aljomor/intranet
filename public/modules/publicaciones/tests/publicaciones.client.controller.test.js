'use strict';

(function() {
	// Publicaciones Controller Spec
	describe('Publicaciones Controller Tests', function() {
		// Initialize global variables
		var PublicacionesController,
		scope,
		$httpBackend,
		$stateParams,
		$location;

		// The $resource service augments the response object with methods for updating and deleting the resource.
		// If we were to use the standard toEqual matcher, our tests would fail because the test values would not match
		// the responses exactly. To solve the problem, we define a new toEqualData Jasmine matcher.
		// When the toEqualData matcher compares two objects, it takes only object properties into
		// account and ignores methods.
		beforeEach(function() {
			jasmine.addMatchers({
				toEqualData: function(util, customEqualityTesters) {
					return {
						compare: function(actual, expected) {
							return {
								pass: angular.equals(actual, expected)
							};
						}
					};
				}
			});
		});

		// Then we can start by loading the main application module
		beforeEach(module(ApplicationConfiguration.applicationModuleName));

		// The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
		// This allows us to inject a service but then attach it to a variable
		// with the same name as the service.
		beforeEach(inject(function($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_) {
			// Set a new global scope
			scope = $rootScope.$new();

			// Point global variables to injected services
			$stateParams = _$stateParams_;
			$httpBackend = _$httpBackend_;
			$location = _$location_;

			// Initialize the Publicaciones controller.
			PublicacionesController = $controller('PublicacionesController', {
				$scope: scope
			});
		}));

		it('$scope.find() should create an array with at least one Publicacione object fetched from XHR', inject(function(Publicaciones) {
			// Create sample Publicacione using the Publicaciones service
			var samplePublicacione = new Publicaciones({
				name: 'New Publicacione'
			});

			// Create a sample Publicaciones array that includes the new Publicacione
			var samplePublicaciones = [samplePublicacione];

			// Set GET response
			$httpBackend.expectGET('publicaciones').respond(samplePublicaciones);

			// Run controller functionality
			scope.find();
			$httpBackend.flush();

			// Test scope value
			expect(scope.publicaciones).toEqualData(samplePublicaciones);
		}));

		it('$scope.findOne() should create an array with one Publicacione object fetched from XHR using a publicacioneId URL parameter', inject(function(Publicaciones) {
			// Define a sample Publicacione object
			var samplePublicacione = new Publicaciones({
				name: 'New Publicacione'
			});

			// Set the URL parameter
			$stateParams.publicacioneId = '525a8422f6d0f87f0e407a33';

			// Set GET response
			$httpBackend.expectGET(/publicaciones\/([0-9a-fA-F]{24})$/).respond(samplePublicacione);

			// Run controller functionality
			scope.findOne();
			$httpBackend.flush();

			// Test scope value
			expect(scope.publicacione).toEqualData(samplePublicacione);
		}));

		it('$scope.create() with valid form data should send a POST request with the form input values and then locate to new object URL', inject(function(Publicaciones) {
			// Create a sample Publicacione object
			var samplePublicacionePostData = new Publicaciones({
				name: 'New Publicacione'
			});

			// Create a sample Publicacione response
			var samplePublicacioneResponse = new Publicaciones({
				_id: '525cf20451979dea2c000001',
				name: 'New Publicacione'
			});

			// Fixture mock form input values
			scope.name = 'New Publicacione';

			// Set POST response
			$httpBackend.expectPOST('publicaciones', samplePublicacionePostData).respond(samplePublicacioneResponse);

			// Run controller functionality
			scope.create();
			$httpBackend.flush();

			// Test form inputs are reset
			expect(scope.name).toEqual('');

			// Test URL redirection after the Publicacione was created
			expect($location.path()).toBe('/publicaciones/' + samplePublicacioneResponse._id);
		}));

		it('$scope.update() should update a valid Publicacione', inject(function(Publicaciones) {
			// Define a sample Publicacione put data
			var samplePublicacionePutData = new Publicaciones({
				_id: '525cf20451979dea2c000001',
				name: 'New Publicacione'
			});

			// Mock Publicacione in scope
			scope.publicacione = samplePublicacionePutData;

			// Set PUT response
			$httpBackend.expectPUT(/publicaciones\/([0-9a-fA-F]{24})$/).respond();

			// Run controller functionality
			scope.update();
			$httpBackend.flush();

			// Test URL location to new object
			expect($location.path()).toBe('/publicaciones/' + samplePublicacionePutData._id);
		}));

		it('$scope.remove() should send a DELETE request with a valid publicacioneId and remove the Publicacione from the scope', inject(function(Publicaciones) {
			// Create new Publicacione object
			var samplePublicacione = new Publicaciones({
				_id: '525a8422f6d0f87f0e407a33'
			});

			// Create new Publicaciones array and include the Publicacione
			scope.publicaciones = [samplePublicacione];

			// Set expected DELETE response
			$httpBackend.expectDELETE(/publicaciones\/([0-9a-fA-F]{24})$/).respond(204);

			// Run controller functionality
			scope.remove(samplePublicacione);
			$httpBackend.flush();

			// Test array after successful delete
			expect(scope.publicaciones.length).toBe(0);
		}));
	});
}());