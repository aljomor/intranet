'use strict';

angular.module('publicaciones').directive('createPublicacion', [
	function() {
		return {
			templateUrl: '/modules/publicaciones/views/create-publicacione.client.view.html',
			restrict: 'E',
			link: function postLink(scope, element, attrs) {

			}
		};
	}
]);

angular.module('publicaciones').directive('listPublicaciones', [
    function() {
        return {
            templateUrl: '/modules/publicaciones/views/list-publicaciones.client.view.html',
            restrict: 'E',
            link: function postLink(scope, element, attrs) {

            }
        };
    }
]);