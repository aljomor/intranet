'use strict';

// Publicaciones controller

angular.module('publicaciones').controller('PublicacionesController', ['$scope', '$stateParams', '$location', 'Authentication', 'Publicaciones', 'PostsRefresh', 'Actualizar', 'createNotification',
	function($scope, $stateParams, $location, Authentication, Publicaciones, PostsRefresh, Actualizar, createNotification) {

		$scope.authentication = Authentication;

		// Create new Publicacione
		$scope.create = function() {

			// Create new Publicacione object
			var publicacione = new Publicaciones ({
				content: this.content

			});

			// Redirect after save

			  publicacione.$save(function(response) {
                response.user = $scope.authentication.user._id;
                response.section = 'my_activity';
                response.action = 'like';
                response.receiver = $scope.authentication.user._id;
                createNotification.create(response);

                $scope.actualizarNoticias();
                // Clear form fields

                $scope.content = '';


			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});

		};

		// Remove existing Publicacione
		$scope.remove = function( publicacione ) {
			if ( publicacione ) { publicacione.$remove();

				for (var i in $scope.publicaciones ) {
					if ($scope.publicaciones [i] === publicacione ) {
						$scope.publicaciones.splice(i, 1);
					}
				}
			} else {
				$scope.publicacione.$remove(function() {
					$location.path('publicaciones');
				});
			}
		};

		// Update existing Publicacione
		$scope.update = function() {
			var publicacione = $scope.publicacione ;

			publicacione.$update(function() {
				$location.path('publicaciones/' + publicacione._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Publicaciones
		$scope.find = function() {
			$scope.publicaciones = Publicaciones.query();

		};

        // update publicaciones
        $scope.actualizar = function() {
            $scope.publicaciones = Publicaciones.query();
            return $scope.publicaciones;

        };

		// Find existing Publicacione
		$scope.findOne = function() {
			$scope.publicacione = Publicaciones.get({ 
				publicacioneId: $stateParams.publicacioneId
			});
        };

        //
        $scope.actualizarNoticias = function(){

            $scope.publicaciones = PostsRefresh.actualizarPublicaciones();
            return $scope.publicaciones;
        };

        // $scope.actualizarComentarios = function(post_object_id, comment_object_id){ };

    }

]);