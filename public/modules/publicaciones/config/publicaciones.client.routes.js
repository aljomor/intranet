'use strict';

//Setting up route
angular.module('publicaciones').config(['$stateProvider',
	function($stateProvider) {
		// Publicaciones state routing
		$stateProvider.
		state('listPublicaciones', {
			url: '/publicaciones',
			templateUrl: 'modules/publicaciones/views/list-publicaciones.client.view.html'
		}).
		state('createPublicacione', {
			url: '/publicaciones/create',
			templateUrl: 'modules/publicaciones/views/create-publicacione.client.view.html'
		}).
		state('viewPublicacione', {
			url: '/publicaciones/:publicacioneId',
			templateUrl: 'modules/publicaciones/views/view-publicacione.client.view.html'
		}).
		state('editPublicacione', {
			url: '/publicaciones/:publicacioneId/edit',
			templateUrl: 'modules/publicaciones/views/edit-publicacione.client.view.html'
		});
	}
]);