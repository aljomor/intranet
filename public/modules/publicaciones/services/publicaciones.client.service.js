'use strict';

//Publicaciones service used to communicate Publicaciones REST endpoints

angular.module('publicaciones').factory('Publicaciones', ['$resource',
    function($resource) {
        return $resource('publicaciones/:publicacioneId', { publicacioneId: '@_id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }

]);

angular.module('publicaciones').service('PostsRefresh', ['$resource','$http','Actualizar',
    function($resource,Publicaciones,Actualizar) {


        this.actualizarPublicaciones = function(){

            var Posti = $resource('/publicaciones');
            Actualizar.publicaciones = Posti.query();

            return Actualizar.publicaciones;
        };
    }
]);

angular.module('publicaciones').factory('Actualizar',
    function() {
        return {
            publicaciones:{}
        };
    }
);

angular.module('publicaciones').service('CommentsRefresh', ['$resource', 'Publicaciones',
    function($resource, Publicaciones) {

        this.actualizarComentarios = function(publicacione){
            var publicacion = Publicaciones.get({
                publicacioneId: publicacione
            });

            return publicacion;
        };
    }
]);
