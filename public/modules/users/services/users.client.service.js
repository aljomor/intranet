'use strict';

// Users service used for communicating with the users REST endpoint
angular.module('users').factory('Users', ['$resource',
	function($resource) {
		return $resource('users', {}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);


// Users service used for communicating with the users REST endpoint
angular.module('users').factory('UserPrivate', ['$resource',
    function($resource) {
        return $resource('users/:userId', {}, {
            update: {
                method: 'PUT'
            }
        });
    }
]);

// Controllers service used for communicating with the controllers REST endpoint
angular.module('users').factory('Controllers', ['$resource',
    function($resource) {

        return $resource('/auth/controllers', {}, {
            update: {
                method: 'PUT'
            }
        });
    }
]);

// Controllers service used for communicating with the controllers REST endpoint
angular.module('users').factory('UsersAdmin', ['$resource',
    function($resource) {

        return $resource('/admin/register', {}, {
            update: {
                method: 'PUT'
            }
        });
    }
]);