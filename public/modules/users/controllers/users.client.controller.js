'use strict';

// Users controller
angular.module('users').controller('UsersController', ['$scope','$anchorScroll', '$stateParams', '$location', 'Authentication', 'Users','Controllers','Groups','UsersAdmin','UserPrivate',
    function($scope,$anchorScroll, $stateParams, $location, Authentication, Users,Controllers,Groups,UsersAdmin,UserPrivate ) {

        $scope.authentication = Authentication;
        $anchorScroll.yOffset = 150;

        // Obtengo los controlladores
        $scope.getControllers = function() {
            $scope.controllers = Controllers.query();
        };

        //Actualizo la lista de permisologia independiente del usuario
        $scope.actualizarPermisos=function(){

            angular.forEach($scope.groups, function(grupo, key) {
                if(grupo._id === $scope.credentials.group){
                    $scope.credentials.access = grupo.acceso;
                }
            });


        };

        $scope.getGroups = function() {
            $scope.groups = Groups.query();
        };
        //funcion para inicializar los datos extrajeros necesarios.
        $scope.init = function(){
            $scope.getGroups();
            $scope.getControllers();
        };


        // Create new Group
        $scope.create = function() {


            // Create new Group object
            var user = new UsersAdmin ({
                firstName: this.credentials.firstName,
                lastName: this.credentials.lastName,
                email: this.credentials.email,
                password:this.credentials.password,
                username:this.credentials.username,
                group: this.credentials.group,
                access:this.credentials.access
            });

            // Redirect after save
            user.$save(function(response) {
                $location.path('/users');

                // Clear form fields


            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        // Remove existing Group
        $scope.remove = function( group ) {
            if ( group ) { group.$remove();

                for (var i in $scope.users ) {
                    if ($scope.users [i] === group ) {
                        $scope.users.splice(i, 1);
                    }
                }
            } else {
                $scope.group.$remove(function() {
                    $location.path('users');
                });
            }
        };

        // Update existing Group
        $scope.update = function() {
            var user = $scope.credentials;


            user.$update(function() {
                $scope.msj = 'Exito! en la modificacion del usuario';
                $scope.gotoAnchor('top');
               // $location.path('users/' + user._id+'/edit');
            }, function(errorResponse) {
                $scope.msj = '';
                $scope.error = errorResponse.data.message;
            });
        };

        // Find a list of Users
        $scope.find = function() {
            $scope.users = Users.query();

        };

        // Find existing Group
        $scope.findOne = function() {
            $scope.credentials = UserPrivate.get({
                userId: $stateParams.userId
            });
        };

        //situa la pantalla en el div correspondiente
        $scope.gotoAnchor = function(x) {
            var newHash = x;
            if ($location.hash() !== newHash) {
                // set the $location.hash to `newHash` and
                // $anchorScroll will automatically scroll to it
                $location.hash(x);
            } else {
                // call $anchorScroll() explicitly,
                // since $location.hash hasn't changed
                $anchorScroll();
            }
        };


    }
]);