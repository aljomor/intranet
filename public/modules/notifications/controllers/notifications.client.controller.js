'use strict';

// Notifications controller
angular.module('notifications').controller('NotificationsController', ['$scope', '$stateParams', '$location', 'Authentication', 'Notifications',
	function($scope, $stateParams, $location, Authentication, Notifications) {
		$scope.authentication = Authentication;

		// Create new Notification
		$scope.create = function() {
			// Create new Notification object
			var notification = new Notifications ({
				name: this.name
			});

			// Redirect after save
			notification.$save(function(response) {
				$location.path('notifications/' + response._id);

				// Clear form fields
				this.name = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Desactivar la Notification
		$scope.remove = function(notification) {

          notification.status = false;
          $scope.notification = notification;

          $scope.update();
		};

        // Marcar como vista la notificacion
        $scope.seen = function(notification) {

          $scope.notification = notification;
          $scope.notification.seen = true;

          $scope.update();
        };

        // Cambia la clase de la notificacion dependiendo si esta vista o no
        $scope.background = function(notification) {

          if(notification.seen === false)
            return 'notseen';
          else
            return 'seen';
        };

		// Update existing Notification
		$scope.update = function() {

			var notification = $scope.notification;

            notification.$update(function() {
				// $location.path('notifications/' + notification._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Notifications
		$scope.find = function() {
			$scope.notifications = Notifications.query();
		};

		// Find existing Notification
		$scope.findOne = function() {
			$scope.notification = Notifications.get({ 
				notificationId: $stateParams.notificationId
			});
		};
	}
]);