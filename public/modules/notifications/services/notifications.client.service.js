'use strict';

//Notifications service used to communicate Notifications REST endpoints
angular.module('notifications').factory('Notifications', ['$resource',
	function($resource) {
		return $resource('notifications/:notificationId', { notificationId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);

angular.module('notifications').service('createNotification', ['$resource', 'Notifications',
    function($resource, Notifications) {

         this.create = function(response){
            var notification = new Notifications ({
              user: response.user, //ersona que le da me gusta o comenta la publicacion
              post: response._id,  //Publicacion
              section: response.section, //A que pertenece la Notificacion. Ej: Publicaciones Propias, Actividad Grupos, Etiquetas, entre otras.
              action: response.action, //Accion que se Notifica. Ej: Me gusta, Comentario, entre otras.
              receiver: response.receiver //Usuario al cual le llega la notificacion.
            });

            notification.$save(function(response){
                //Accion al guardar notificacion
            }, function(errorResponse) {
                // $scope.error = errorResponse.data.message;
            });
         };
    }
]);