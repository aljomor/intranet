'use strict';

angular.module('notifications').directive('listNotification', [
	function() {
		return {
			templateUrl: '/modules/notifications/views/list-notifications.client.view.html',
			restrict: 'E',
			link: function postLink(scope, element, attrs) {
			}
		};
	}
]);