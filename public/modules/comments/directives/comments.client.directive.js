'use strict';

angular.module('comments').directive('createComment', [
	function() {
		return {
			templateUrl: '/modules/comments/views/create-comment.client.view.html',
			restrict: 'E',
			link: function postLink(scope, element, attrs) {

			}
		};
	}
]);

angular.module('comments').directive('listComments', [
    function() {
        return {
            templateUrl: '/modules/comments/views/list-comments.client.view.html',
            restrict: 'E',
            link: function postLink(scope, element, attrs) {

            }
        };
    }
]);