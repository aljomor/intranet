'use strict';

// Comments controller
angular.module('comments').controller('CommentsController', ['$scope', '$stateParams', '$location', 'Authentication', 'Comments', 'Publicaciones', 'Actualizar', 'CommentsRefresh',
    function($scope, $stateParams, $location, Authentication, Comments, Publicaciones, Actualizar, CommentsRefresh) {
        $scope.authentication = Authentication;

        //funcion q asigna el valor de la publicacion a el comentario como clave foranea
        $scope.asignarPost = function(post_object_id){
            this.content = '';
            this.post = post_object_id;
        };

        // Create new Comment
        $scope.create = function() {
            // Create new Comment object
            var comment = new Comments ({
                content: this.content,
                post: this.post
            });


            // Redirect after save
            comment.$save(function(response,publicaciones) {

                $scope.actualizarPublicacion(response);

                // Clear form fields
                $scope.content = '';
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        //Actualizar comentarios de la Publicacion
        $scope.actualizarPublicacion = function(response){

            var publicacion = Publicaciones.get({
                publicacioneId: response.post
            }, function() {

                //preparacion de la publicacion
                if (publicacion.comments.length === 4) publicacion.comments.splice(0, 1);

                publicacion.comments.push({ comment : response._id });

                publicacion.user = { id:$scope.authentication.user._id };

                //Modificar
                Publicaciones.update({ publicacioneId:publicacion._id }, publicacion,
                    function(){
                    $scope.actualizarComments();
                });
            });
        };

        // Remove existing Comment
        $scope.remove = function(comment) {
            if ( comment ) {
                comment.$remove();

                for (var i in $scope.comments) {
                    if ($scope.comments [i] === comment) {
                        $scope.comments.splice(i, 1);
                    }
                }
            } else {
                $scope.comment.$remove(function() {
                    $location.path('comments');
                });
            }
        };

        // Update existing Comment
        $scope.update = function() {
            var comment = $scope.comment;

            comment.$update(function() {
                // $location.path('comments/' + comment._id);
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        // Find a list of Comments
        $scope.find = function() {
            $scope.comments = Comments.query();
        };

        // Find existing Comment
        $scope.findOne = function() {
            $scope.comment = Comments.get({
                commentId: $stateParams.commentId
            });
        };

        //actualiza los comentarios
        $scope.actualizarComments = function(){

             Publicaciones.get({
                publicacioneId: $scope.publicacione._id
            }).$promise.then(function(response){
                    $scope.publicacione.comments = response.comments;
                });

        };

    }
]);